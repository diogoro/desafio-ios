//
//  ListaRepositorioViewControllerTests.swift
//  DesafioConcreteTests
//
//  Created by Diogo Ribeiro de Oliveira on 05/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import XCTest
@testable import DesafioConcrete

class ListaRepositoriosViewControllerTests: XCTestCase {
    
    var sut: ListaRepositoriosViewController?
    
    override func setUp() {
        super.setUp()
        sut = StoryboardScene.Main.listaRepositoriosViewController.instantiate()
        _ = sut?.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        sut = nil
    }
    
    func testNotNil() {
        XCTAssertNotNil(sut)
    }
    
}
