//
//  ListaPullRequestsViewControllerTests.swift
//  DesafioConcreteTests
//
//  Created by Diogo Ribeiro de Oliveira on 05/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import XCTest
@testable import DesafioConcrete

class ListaPullRequestsViewControllerTests: XCTestCase {
    
    var sut: ListaPullRequestsViewController?
    
    override func setUp() {
        super.setUp()
        sut = StoryboardScene.Main.listaPullRequestsViewController.instantiate()
        sut?.nomeRepositorioSelecionado = "ReactiveX/RxJava"
        _ = sut?.view
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func testNotNil() {
        XCTAssertNotNil(sut)
    }
    
}
