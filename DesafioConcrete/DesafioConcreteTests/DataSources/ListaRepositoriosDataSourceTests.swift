//
//  ListaRepositoriosDataSourceTests.swift
//  DesafioConcreteTests
//
//  Created by Diogo Ribeiro de Oliveira on 05/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import DesafioConcrete

class ListaRepositoriosDataSourceTests: XCTestCase {
    
    var sut: ListaRepositoriosDataSource?
    var delegate: ListaRespositoriosTableDelegate?
    var items: [Repositorio] = []
    var fakeTableView = UITableView(frame: CGRect.zero)
    
    override func setUp() {
        super.setUp()
        let controller = StoryboardScene.Main.listaRepositoriosViewController.instantiate()
        delegate = ListaRespositoriosTableDelegate(controller)
        sut = ListaRepositoriosDataSource(itens: items, tableView: fakeTableView, delegate: delegate!)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        sut = nil
        delegate = nil
    }
    
    func testNotNil() {
        XCTAssertNotNil(sut)
    }
    
    func testCellSetupHeight() {
        XCTAssertEqual(RepositorioTableViewCell.setupHeight(), 130)
    }
    
    func testNumberOfRows() {
        XCTAssertEqual(fakeTableView.numberOfRows(inSection: 0), items.count)
    }
    
}
