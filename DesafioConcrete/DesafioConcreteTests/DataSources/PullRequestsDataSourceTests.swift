//
//  PullRequestsDataSourceTests.swift
//  DesafioConcreteTests
//
//  Created by Diogo Ribeiro de Oliveira on 05/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import DesafioConcrete

class PullRequestsDataSourceTests: XCTestCase {
    
    var sut: PullRequestDataSource?
    var delegate: PullRequestTableDelegate?
    var items: [PullRequest] = []
    var fakeTableView = UITableView(frame: CGRect.zero)
    
    override func setUp() {
        super.setUp()
        let controller = StoryboardScene.Main.listaPullRequestsViewController.instantiate()
        delegate = PullRequestTableDelegate(controller)
        sut = PullRequestDataSource(itens: items, tableView: fakeTableView, delegate: delegate!)
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
        delegate = nil
    }
    
    func testNotNil() {
        XCTAssertNotNil(sut)
    }
    
    func testCellSetupHeight() {
        XCTAssertEqual(PullRequestTableViewCell.setupHeight(), 135)
    }
    
    func testNumberOfRows() {
        XCTAssertEqual(fakeTableView.numberOfRows(inSection: 0), items.count)
    }
}
