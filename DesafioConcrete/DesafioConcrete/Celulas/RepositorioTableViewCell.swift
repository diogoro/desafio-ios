//
//  RepositorioTableViewCell.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 02/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit
import Reusable
import AlamofireImage

class RepositorioTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var lbNomeRepositorio: UILabel!
    @IBOutlet weak var lbDescricaoRepositorio: UILabel!
    @IBOutlet weak var lbUsername: UILabel!
    @IBOutlet weak var lbNumeroForks: UILabel!
    @IBOutlet weak var lbNumeroEstrelas: UILabel!
    @IBOutlet weak var imvImagemRepositorio: UIImageView!
    
    static func setupHeight() -> CGFloat {
        return 130
    }
    
    func setup(item: Repositorio) {
        lbNomeRepositorio.text = item.fullName
        lbDescricaoRepositorio.text = item.description
        lbNumeroForks.text = "\(item.forksCount ?? 0)"
        lbNumeroEstrelas.text = "\(item.stargazersCount ?? 0)"
        guard let dono = item.owner else {
            return
        }
        lbUsername.text = dono.login
        guard let urlImage = URL(string: dono.avatarUrl ?? "") else {
            return
        }
        imvImagemRepositorio.af_setImage(withURL: urlImage, placeholderImage: nil, filter: CircleFilter(), progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.crossDissolve(1.0), runImageTransitionIfCached: true, completion: nil)
    }
    
}
