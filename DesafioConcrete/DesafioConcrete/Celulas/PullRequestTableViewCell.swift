//
//  PullRequestTableViewCell.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 02/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit
import Reusable
import AlamofireImage

class PullRequestTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbDescicao: UILabel!
    @IBOutlet weak var lbUsername: UILabel!
    @IBOutlet weak var lbData: UILabel!
    @IBOutlet weak var imvDono: UIImageView!
    
    
    static func setupHeight() -> CGFloat {
        return 135
    }
    
    func setup(item: PullRequest) {
        self.lbTitulo.text = item.title
        self.lbDescicao.text = item.body
        self.lbData.text = item.createdAt
        guard let dono = item.user else {
            return
        }
        self.lbUsername.text = dono.login
        
        guard let urlImage = URL(string: dono.avatarUrl ?? "") else {
            return
        }
        
        imvDono.af_setImage(withURL: urlImage, placeholderImage: nil, filter: CircleFilter(), progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.crossDissolve(1.0), runImageTransitionIfCached: true, completion: nil)
    }
    
}
