//
//  ListaReposioriosDataSource.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 03/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit

final class ListaRepositoriosDataSource: NSObject, TableViewDataSource {
    
    var itens: [Repositorio] = []
    weak var tableView: UITableView?
    weak var delegate: UITableViewDelegate?
    
    init(itens: [T], tableView: UITableView, delegate: UITableViewDelegate) {
        self.itens = itens
        self.tableView = tableView
        self.delegate = delegate
        super.init()
        tableView.register(cellType: RepositorioTableViewCell.self)
        self.setupTableView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: RepositorioTableViewCell.self)
        let repo = itens[indexPath.row]
        cell.setup(item: repo)
        return cell
    }
    
}

class ListaRespositoriosTableDelegate: NSObject, UITableViewDelegate {
    
    let delegate: ListaRespositoriosDelegate
    
    init(_ delegate: ListaRespositoriosDelegate) {
        self.delegate = delegate
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RepositorioTableViewCell.setupHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        delegate.repositorioSelecionado(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        delegate.carregaProximaPagina(index: indexPath)
    }
}
