//
//  PullRequestDataSource.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 04/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit

final class PullRequestDataSource: NSObject, TableViewDataSource {
    
    var itens: [PullRequest] = [] //Colocar o tipo correto
    weak var tableView: UITableView?
    weak var delegate: UITableViewDelegate?
    
    init(itens: [T], tableView: UITableView, delegate: UITableViewDelegate) {
        self.itens = itens
        self.tableView = tableView
        self.delegate = delegate
        super.init()
        tableView.register(cellType: PullRequestTableViewCell.self)
        self.setupTableView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: PullRequestTableViewCell.self)
        let pull = itens[indexPath.row]
        cell.setup(item: pull)
        return cell
    }
}

class PullRequestTableDelegate: NSObject, UITableViewDelegate {
    
    let delegate: PullRequestDelegate
    
    init(_ delegate: PullRequestDelegate) {
        self.delegate = delegate
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PullRequestTableViewCell.setupHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        delegate.pullRequestSelecionado(at: indexPath)
    }
}
