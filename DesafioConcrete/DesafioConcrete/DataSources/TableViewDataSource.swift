//
//  TableViewDataSource.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 03/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit

protocol TableViewDataSource: UITableViewDataSource {
    associatedtype T
    var itens:[T] {get}
    weak var tableView: UITableView? {get}
    weak var delegate: UITableViewDelegate? {get}
    
    init(itens: [T], tableView: UITableView, delegate: UITableViewDelegate)
    
    func setupTableView()
}

extension TableViewDataSource {
    func setupTableView() {
        self.tableView?.dataSource = self
        self.tableView?.delegate = self.delegate
        self.tableView?.reloadData()
    }
}
