//
//  PullRequest.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 03/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import Foundation
import SwiftyJSON

class PullRequest {
    
    var id: Int?
    var title: String?
    var body: String?
    var htmlUrl: String?
    var createdAt: String?
    var state: String?
    var user: Dono?
    
    init(json: JSON) {
        self.id = json["id"].int
        self.title = json["title"].string
        self.body = json["body"].string
        self.htmlUrl = json["html_url"].string
        self.createdAt = json["created_at"].dateTimeISO8601
        self.state = json["state"].string
        self.user = Dono(json: json["user"])
        
    }
}

extension JSON {
    public var dateTimeISO8601: String? {
        get {
            switch self.type {
            case .string:
                return formatterDateTimeISO8601(dateISO8601: self.object as! String)
            default:
                return nil
            }
        }
    }
    
    private func formatterDateTimeISO8601(dateISO8601: String) -> String {
        let dateFormatter = DateFormatter()
        if #available(iOS 10.0, *) {
            let iso8601Formatter = ISO8601DateFormatter()
            //iso8601Formatter.formatOptions =
            guard let date = iso8601Formatter.date(from: dateISO8601) else {
                return dateISO8601
            }
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            let dateString = dateFormatter.string(from: date)
            return dateString
        } else {
            let tempLocale = dateFormatter.locale
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            guard let date = dateFormatter.date(from: dateISO8601) else {
                return dateISO8601
            }
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            dateFormatter.locale = tempLocale
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
    }
}
