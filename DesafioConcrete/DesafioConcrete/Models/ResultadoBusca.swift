//
//  ResultadoBusca.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 03/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import Foundation
import SwiftyJSON

class ResultadoBusca {
    var totalCount: Int?
    var incompleteStatus: Bool?
    var repositores: [Repositorio] = []
    
    init(json: JSON) {
        self.incompleteStatus = json["incomplete_results"].bool
        self.totalCount = json["total_count"].int
        if let array = json["items"].array {
            for element in array {
                let repo = Repositorio(json: element)
                self.repositores.append(repo)
            }
        }
    }
    
}

