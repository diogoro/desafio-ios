//
//  Alerta.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 05/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit

class Alerta {
    
    let alerta = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
    
    init(titulo:String, mensagem:String) {
        alerta.title = titulo
        alerta.message = mensagem
        let botaoFechar = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alerta.addAction(botaoFechar)
    }
    
    func getAlerta() -> UIAlertController {
        return alerta
    }
}
