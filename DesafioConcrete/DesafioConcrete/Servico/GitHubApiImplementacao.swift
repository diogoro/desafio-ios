//
//  GitHubApiImplementacao.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 04/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum Result<T> {
    case success(T)
    case error(Error)
}

typealias RetornoRepositorios = (Result<[Repositorio]>) -> Void
typealias RetornoPullRequests = (Result<[PullRequest]>) -> Void

protocol GitHubAPICalls {
    func buscaRepositorios(query: String, completion: @escaping RetornoRepositorios)
    func buscaPullRequestRepositorio(repositorio: String, completion: @escaping RetornoPullRequests)
}

class GitHubAPICallsImpl: GitHubAPICalls {
    func buscaRepositorios(query: String, completion: @escaping RetornoRepositorios) {
        let endereco = "https://api.github.com/search/repositories?q=\(query)"
        chamadaAlamofire(metodoHttp: HTTPMethod.get, endereco: endereco) { (resposta) in
            switch resposta {
            case .success(let json):
                let results = ResultadoBusca(json: json)
                completion(.success(results.repositores))
            case .error(let erro):
                completion(.error(erro))
            }
        }
    }
    
    func buscaPullRequestRepositorio(repositorio: String, completion: @escaping RetornoPullRequests) {
        let endereco = "https://api.github.com/repos/\(repositorio)/pulls?state=all"
        chamadaAlamofire(metodoHttp: HTTPMethod.get, endereco: endereco) { (resposta) in
            switch resposta {
            case .success(let json):
                var pulls: [PullRequest] = []
                let array = json.array
                if let arrayAux = array {
                    for element in arrayAux {
                        let pull = PullRequest(json: element)
                        pulls.append(pull)
                    }
                }
                completion(.success(pulls))
            case .error(let erro):
                completion(.error(erro))
            }
        }
    }
    
    fileprivate func chamadaAlamofire (metodoHttp: Alamofire.HTTPMethod, endereco: String, parametros: [String:Any]? = nil, cabecalhosHttp:[String:String]? = nil, completion: @escaping (Result<JSON>) -> Void) {
        Alamofire.request(endereco, method: metodoHttp, parameters: parametros, encoding: URLEncoding.default, headers: cabecalhosHttp).responseJSON { (resposta) in
            if resposta.result.isSuccess {
                guard let value = resposta.result.value else {
                    print("")
                    return
                }
                let json = JSON(value)
                completion(.success(json))
            } else if resposta.result.isFailure {
                guard let erro = resposta.error else {
                    print("Ocorreu uma falha mas o objeto error está nil")
                    return
                }
                completion(.error(erro))
            }
        }
    }
}


