//
//  ListaPullRequestsViewController.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 02/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit
import AlamofireNetworkActivityIndicator

protocol PullRequestDelegate {
    func pullRequestSelecionado(at index: IndexPath)
}

class ListaPullRequestsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbInfos: UILabel!
    
    var nomeRepositorioSelecionado: String!
    var pullRequests: [PullRequest] = []
    var opened = 0
    var closed = 0
    
    let api:GitHubAPICalls = GitHubAPICallsImpl()
    
    var pullRequestDataSource: PullRequestDataSource?
    var pullRequestDelegate: PullRequestTableDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuraNavigationBar()
        NetworkActivityIndicatorManager.shared.isEnabled = true
        api.buscaPullRequestRepositorio(repositorio: nomeRepositorioSelecionado) { (resposta) in
            NetworkActivityIndicatorManager.shared.isEnabled = false
            switch resposta {
            case .success(let pullRequests):
                self.configuraTableview(pullRequests: pullRequests)
            case.error(let erro):
                print(erro)
                let alerta = Alerta(titulo: "Error", mensagem: "Ocorreu um erro: \(erro.localizedDescription)")
                self.present(alerta.getAlerta(), animated: true, completion: nil)
            }
        }
        
    }
    
    func configuraNavigationBar() {
        navigationItem.title = nomeRepositorioSelecionado
        guard let nav = navigationController else {
            return
        }
        nav.navigationBar.tintColor = UIColor.white
    }
    
    func configuraTableview(pullRequests: [PullRequest]) {
        self.pullRequests = pullRequests
        configurarLbInfos()
        pullRequestDelegate = PullRequestTableDelegate(self)
        pullRequestDataSource = PullRequestDataSource(itens: pullRequests, tableView: self.tableView, delegate: pullRequestDelegate!)
    }
    
    func configurarLbInfos() {
        for pull in self.pullRequests {
            if pull.state == "open" {
                opened = opened + 1
            } else if pull.state == "closed" {
                closed = closed + 1
            }
        }
        let infos = "\(opened) opened /\(closed) closed"
        let textoPreto = "/\(closed) closed"
        self.lbInfos.text = infos
        let myMutableString = NSMutableAttributedString(string: infos)
        let range: NSRange = myMutableString.mutableString.range(of: textoPreto, options: .caseInsensitive)
        myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: range)
        self.lbInfos.attributedText = myMutableString
    }


}

extension ListaPullRequestsViewController: PullRequestDelegate {
    func pullRequestSelecionado(at index: IndexPath) {
        let pullSelecionado = pullRequests[index.row]
        guard let pullHtmlUrl = URL(string: pullSelecionado.htmlUrl ?? "") else {
            let alerta = Alerta(titulo: "Erro", mensagem: "Pull Request não possui url para exibição no browser")
            self.present(alerta.getAlerta(), animated: true, completion: nil)
            return
        }
        if UIApplication.shared.canOpenURL(pullHtmlUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(pullHtmlUrl, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(pullHtmlUrl)
            }
        }
    }
}
