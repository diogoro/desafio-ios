//
//  ListaRepositoriosViewController.swift
//  DesafioConcrete
//
//  Created by Diogo Ribeiro de Oliveira on 02/11/17.
//  Copyright © 2017 Diogo Ribeiro de Oliveira. All rights reserved.
//

import UIKit
import AlamofireNetworkActivityIndicator

protocol ListaRespositoriosDelegate {
    func repositorioSelecionado(at index: IndexPath)
    func carregaProximaPagina(index: IndexPath)
}

class ListaRepositoriosViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let api:GitHubAPICalls = GitHubAPICallsImpl()
    
    var listaRepositoriosDataSource: ListaRepositoriosDataSource?
    var listaRepositorioDelegate: ListaRespositoriosTableDelegate?
    
    var repositorios:[Repositorio] = []
    var pagina: Int = 1
    var acabouAsPaginas = false
    let baseQuery = "language:Java&sort=stars&page="
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carregarProximaPagina()     
        // Do any additional setup after loading the view.
    }
    
    func carregarProximaPagina() {
        if !acabouAsPaginas {
            NetworkActivityIndicatorManager.shared.isEnabled = true
            let query = baseQuery + "\(pagina)"
            api.buscaRepositorios(query: query, completion: { (resposta) in
                NetworkActivityIndicatorManager.shared.isEnabled = false
                switch resposta {
                case .success(let repositorios):
                    self.configuraTableView(repositorios: repositorios)
                    self.pagina = self.pagina + 1
                case .error(let erro):
                    self.acabouAsPaginas = true
                    print(erro)
                    let alerta = Alerta(titulo: "Error", mensagem: "Ocorreu um erro: \(erro.localizedDescription)")
                    self.present(alerta.getAlerta(), animated: true, completion: nil)
                }
            })
        }
    }
    
    func configuraTableView(repositorios: [Repositorio]) {
        self.repositorios.append(contentsOf: repositorios)
        listaRepositorioDelegate = ListaRespositoriosTableDelegate(self)
        listaRepositoriosDataSource = ListaRepositoriosDataSource(itens: self.repositorios, tableView: self.tableView, delegate: listaRepositorioDelegate!)
    }

}

extension ListaRepositoriosViewController: ListaRespositoriosDelegate {
    func repositorioSelecionado(at index: IndexPath) {
        //Como navegar entre as pelas do storyboard
        let nextController = StoryboardScene.Main.listaPullRequestsViewController.instantiate()
        
        let repositorioSelecionado = repositorios[index.row]
        nextController.nomeRepositorioSelecionado = repositorioSelecionado.fullName
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func carregaProximaPagina(index: IndexPath) {
        if self.repositorios.count - 1 == index.row {
            self.carregarProximaPagina()
        }
    }
}
